import org.kde.kirigami 2.19 as Kirigami

Kirigami.GlobalDrawer {
    id: root

    isMenu: true
    actions: [
        Kirigami.Action {
            text: "Action"
            onTriggered: {
                console.log("Action triggered");
            }
        }
    ]
}
